FROM debian:buster-slim
LABEL maintainer="bubu@bubu1.eu"

RUN apt-get update && apt-get install -qy --no-install-recommends \
    openssh-client \
    locales \
    python3 \
    mercurial \
    git \
    git-svn \
    bzr \
    curl \
    ca-certificates \
    python3-git \
    python3-pyasn1 \
    python3-pyasn1-modules \
    python3-yaml \
    python3-ruamel.yaml \
    python3-requests \
    python3-pil \
    python3-paramiko \
    python3-qrcode \
    && apt-get clean

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && locale-gen
ENV LANG en_US.UTF-8 

COPY test /
